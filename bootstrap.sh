#!/bin/env bash

echo "Block devices: "
lsblk
echo -e "\n"

echo -n "Device to setup: "
read DISK
echo -n "LUKS password: "
read LUKS_PASS
echo -n "Root password: "
read ROOT_PASS
echo -n "Hostname: "
read HOSTNAME

function prepare_partitions_efi() {
	echo "Creating GPT layout..."
	parted -s $DISK mklabel gpt
	parted -s $DISK "mkpart fat32 2048s 1050623s"
	parted -s $DISK "set 1 esp on"
	parted -s $DISK "mkpart ext4 1050624s 100%"

	BOOT_PARTITION=$(ls $DISK*1)
	CRYPTED_ROOT_PARTITION=$(ls $DISK*2)

	echo "Setting up encryption..."
	echo -n $LUKS_PASS | cryptsetup --batch-mode --cipher=aes-xts-plain64 --key-size=512 luksFormat $CRYPTED_ROOT_PARTITION
	echo -n $LUKS_PASS | cryptsetup open $CRYPTED_ROOT_PARTITION crypted 

	echo "Creating file systems..."
	yes | mkfs.fat -F32 $BOOT_PARTITION
	yes | mkfs.ext4 -q -L root /dev/mapper/crypted

	echo "Mounting..."
	mount /dev/mapper/crypted /mnt
	mkdir /mnt/boot
	mount $BOOT_PARTITION /mnt/boot
	lsblk $DISK
}

function bootstrap_arch_efi() {
	pacstrap /mnt base base-devel linux linux-firmware
	genfstab -U /mnt >> /mnt/etc/fstab

	arch-chroot /mnt /bin/bash <<-EOF
		echo "Updating repo cache..."
		pacman -Sy
		echo "Setting up time & date..." ln -sfv /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
		ln -sfv /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
		hwclock --systohc

		echo "Setting up locales..."
		echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
		echo "pl_PL.UTF-8 UTF-8" >> /etc/locale.gen
		locale-gen
		echo "LANG=en_US.UTF-8" > /etc/locale.conf
		echo "KEYMAP=pl" > /etc/vconsole.conf

		echo "Setting up hostname..."
		echo $HOSTNAME > /etc/hostname

		echo "Preparing encryption HOOKS..."
		sed -i 's/^HOOKS.*/HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block sd-encrypt filesystems fsck)/' /etc/mkinitcpio.conf
		mkinitcpio -p linux

		echo "Setting up root password..."
		echo "root:$ROOT_PASS" | chpasswd
	EOF
}

function setup_systemd_boot() {
	echo "Setting up systemd boot on $DISK..."
	arch-chroot /mnt /bin/bash <<-EOF
		bootctl install
	EOF
	cat <<-EOF > /mnt/boot/loader/entries/arch.conf
		title Arch Linux
		linux /vmlinuz-linux
		initrd /initramfs-linux.img
		options rd.luks.name=CRYPTED=crypted root=/dev/mapper/crypted rw
	EOF
	CRYPTED_UUID=$(blkid $CRYPTED_ROOT_PARTITION -o value | head -n 1)
	arch-chroot /mnt /bin/bash <<-EOF
		sed -i "s|CRYPTED|${CRYPTED_UUID}|" /boot/loader/entries/arch.conf
	EOF
}

function cleanup() {
	umount -R /mnt
	cryptsetup close crypted
}

if [ -d /sys/firmware/efi ]
then
	echo "UEFI setup..."
	prepare_partitions_efi
	bootstrap_arch_efi
	setup_systemd_boot
	cleanup
else
	echo "Legacy setup is not supported anymore ... Buy hardware with (U)EFI support."
fi


